package j8;

import java.util.function.Consumer;

//@FunctionalInterface
//public class MyConsumer<T> {
//
//    void accept(T t);
//
//    default MyConsumer<T> andThen(MyConsumer<T> other){
//
//        return (T t) -> {
//            this.accept(t);
//            other.accept(t);
//        }
//    }
//}
