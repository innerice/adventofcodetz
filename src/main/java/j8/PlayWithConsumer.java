package j8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class PlayWithConsumer {
    public static void main(String[] args) {
        Consumer<String> consumer = System.out::println;

        consumer.accept("Jakis string");


        List<String> stringList = new ArrayList<>(Arrays.asList("Kasia","Ania","Basia"));

        Consumer<List<String>> listConsumer = List::clear;

        listConsumer.accept(stringList);

        System.out.println(stringList.size());

//
//        MyConsumer<List<String>> addConsumer = (List<String> t) ->t.add("test");
//        MyConsumer<List<String>> secondAddConsumer = (List<String> t) ->t.add("Hello");
//
//        MyConsumer<List<String>> groupConsumer = addConsumer.andThen(secondAddConsumer);
//
//        groupConsumer.accept(stringList);
//        System.out.println(stringList);





    }

}
