package j8;

import model.ContractType;
import model.Employee;
import org.w3c.dom.ls.LSOutput;

import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.*;

public class PlayWithStreams {
    public static void main(String[] args) {

        List<Employee> employees = j8.stream.FileUtils.load(Path.of("src/main/resources/employees.csv"));

        ex1(employees);
        ex2(employees);
        ex3(employees);
        ex4(employees);
        ex5(employees);
        ex6(employees);
        ex7(employees);
        ex8(employees);
        ex9(employees);
        ex10(employees);
        ex11(employees);
        ex12(employees);
        ex13(employees);
        ex14(employees);
        ex14a(employees);
        ex14c(employees);
        ex15(employees);

    }

    private static void ex15(List<Employee> employees) {
        Optional<Employee> collect = employees.stream()
                .collect(
                        Collectors.maxBy(
                                Comparator.comparing(e -> e.getSalary())
                        )
                );
        System.out.println(collect);

        System.out.println();
    }

    private static void ex14c(List<Employee> employees) {
        Map<Object, Long> a = employees.stream()
                .collect(
                        groupingBy(
                                e -> e.getFirstName(),
                                filtering(
                                        e -> e.getFirstName().endsWith("a"),
                                        counting()
                                )
                        )
                );
        System.out.println(a);
    }

    private static void ex14a(List<Employee> employees) {
        Map<String, Long> collect = employees.stream()
                .collect(
                        groupingBy(
                                e -> e.getFirstName(),
                                Collectors.counting()
                        )
                );
        System.out.println(collect);


    }

    private static void ex14(List<Employee> employees) {
        Map<String, List<Employee>> collect = employees.stream()
                .collect(
                        groupingBy(
                                e -> e.getFirstName()
                        )
                );
        System.out.println(collect);

    }

    private static void ex13(List<Employee> employees) {
        Map<String, Double> collect = employees.stream()
                .collect(
                        Collectors.groupingBy(
                                e -> e.getType().name(),
                                averagingDouble(
                                        e -> e.getSalary()

                                )
                        )
                );
        System.out.println(collect);
    }

    //zad12
    private static void ex12(List<Employee> employees) {
        Map<Boolean, Long> collect = employees.stream()
                .collect(
                        Collectors.partitioningBy(
                                e -> e.getSalary() > 5000,
                                Collectors.counting()
                        ));
        System.out.println(collect);


    }

    //zad11
    private static void ex11(List<Employee> employees) {
        String collect = employees.stream()
                .map(e -> e.getLastName())
                .collect(Collectors.joining(","));
        System.out.println(collect);

    }

    //zad10
    private static void ex10(List<Employee> employees) {
        employees.stream()
                .sorted(
                        Comparator.comparing(Employee::getLastName).thenComparing(Employee::getFirstName))
                .forEach(System.out::println);
    }

    //zad 9
    private static void ex9(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getFirstName().charAt(2) == 'a')
                .filter(e -> e.getLastName().charAt(2) == 'b')
                .forEach(System.out::println);


    }

    //zad8
    private static void ex8(List<Employee> employees) {
        Map<String, List<Double>> collect = employees.stream()
                .collect(
                        Collectors.groupingBy(
                                Employee::getLastName,
                                Collectors.mapping(
                                        e -> e.getSalary() * 1.12, toList()
                                )
                        )
                );
    }
    //zad 7

    private static void ex7(List<Employee> employees) {
        boolean kowalska = employees.stream()
                .anyMatch(e -> e.getLastName().equals("Kowalska"));
        System.out.println(kowalska);
    }

    //zad 6
    private static void ex6(List<Employee> employees) {
        employees.stream()
                .map(e -> e.getProfession())
                .map(e -> e.toUpperCase())
                .forEach(System.out::println);
    }

    //zad5
    private static void ex5(List<Employee> employees) {
        employees.stream()
                .map(e -> e.getLastName())
                .map(e -> e.substring(e.length() - 3))
                .map(e -> e.endsWith("ski") || e.endsWith("ska") ? e.toUpperCase() : e)
                .forEach(System.out::println);


    }
    //zad4

    private static void ex4(List<Employee> employees) {
        employees.stream()
                .map(employee -> employee.getFirstName())
                .forEach(System.out::println);
    }

    //zad3
    private static void ex3(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getLastName().endsWith("ska"))
                .filter(e -> e.getType().equals(ContractType.F))
                .forEach(System.out::println);

    }


    //zad 2
    private static void ex2(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getAge() % 2 == 0)
                .forEach(System.out::println);
    }

    //zad1
    private static void ex1(List<Employee> employees) {
        employees.stream()
                .filter(employee -> employee.getSalary() > 2500
                        && employee.getSalary() < 3199)
                .forEach(System.out::println);

    }


}
