package model;

public enum ContractType {
    F("FULL"),
    H("HALF");

    String description;
    ContractType(String desc){
        this.description=desc;
    }
    public String getDescription(){
        return description;
    }
}
