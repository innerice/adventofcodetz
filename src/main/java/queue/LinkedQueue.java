package queue;

public class LinkedQueue<T> implements Queue<T>{

    private Link<T> head;

    //sprawdzic czy mmay czolo kolejki
    //jesli nie to ustawic na nim nowy link
    //jesli mamy czolo kolejki to w petli przebiegamy
    //po nastepnych ogniwach az dojedziemy do pustego
    //czyli daj nastepnego nic nie zwracaj
    //[Link,Link,Link] <-- offer 50
    //[Link -> daj nastepnego, Link -> daj nastepnego, ...]
    //
    @Override
    public void offer(T element) {
     Link<T> newLink = new Link<>(element);
        if (head == null){
         head = newLink;
     }  else {
            //mamyczolo, przebiegamy po petli
            //dopoki badane ogniwo ma kolejny element
            //idziemy do "ogona"
        Link<T> link = this.head;
        while (link.getNext() != null){
            link = link.getNext();
        }
        link.setNext(newLink);

        }
    }

    @Override
    public T poll() {
        if (head == null){
            System.out.println("Empty queue");
            return null;
            }
        //jezeli jest to pobieramy wartosc head'a
        T headValue = head.getValue();
        head = head.getNext();
        return headValue;
    }

    @Override
    public T peek() {
        return (T) head.getValue();
    }

    @Override
    public int size() {
        return 0;
    }
}
