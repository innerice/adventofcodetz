package queue;

import java.lang.reflect.Array;
import java.util.Arrays;

public class ArrayQueue<T> implements Queue<T> {

        private Object[] elements;
    private int newLength;

    public ArrayQueue(){
            this.elements = new Object[0];
        }

        //[1] , 2 , 3
    //Kopia tablicy gdzie jej wielkosc bedzie zawsze sieze+1
    //Arrays.copyOf()
    //elements[elements.lenght -1] - element;
    @Override
    public void offer(T element) {
        element = Arrays.copyOf(
            elements, newLength: elements.length +1);
    elements[elements.length -1] = element;
    }

    @Override
    public T poll() {
        if (checkIfArrayIsEmpty()) return null;
        T element = (T) elements[0];
        elements = Arrays.copyOfRange(
                elements,1,elements.length);
        return element;
    }

    @Override
    public T peek() {
        if (checkIfArrayIsEmpty()) return null;
        return (T) elements[0];
    }

    private boolean checkIfArrayIsEmpty() {
        if (elements.length == 0) {
            System.out.println("Queue is empty");
            return true;
        }
        return false;
    }

    @Override
    public int size() {
        return 0;
    }
}
