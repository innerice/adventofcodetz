package hospital;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Witaj w rejestracji \n:");
        String option;
        HospitalQueueService hospitalQueueService = new HospitalQueueService();
        do {
            printMenu();
            option = scanner.nextLine();
            if ("a".equals(option)) {
                System.out.println("Rejestracja nowego pacjenta");
                Patient newPatient = handleNewPatient(scanner);
                hospitalQueueService.addPatient(newPatient);
            } else if ("b".equals(option)) {
                Patient handledPatient = hospitalQueueService.handlePatient();
                printPatientInfo(handledPatient);
            } else if ("c".equals(option)) {

            } else if ("d".equals(option)) {
            }
        } while (!"q".equals(option));
    }


    private static void printMenu(){
        System.out.println(
                "a.Rejestracja nowego pascjenta  \n" +
                        "b.Obłóż pacjenta \n" +
                        "c.Ilość pacjentów w kolejce   \n" +
                        "d. Nastepony pacjent   \n" +
                        "q. Wyjście");
    }
    private static void printPatientInfo(Patient handledPatient){
        System.out.println(
                new StringBuilder()
                .append("Pacjent  ")
                .append(handledPatient.getName())
                .append("   ")
                .append(handledPatient.getSurname())
                .append("    został przyjety")
                .toString()
        );
    }

    private static Patient handleNewPatient(Scanner scanner) {
        System.out.println("Imie:");
        String name = scanner.nextLine();
        System.out.println("Nazwisko:");
        String surname = scanner.nextLine();
        System.out.println("Złość:");
        int howAngry = Integer.valueOf(scanner.nextLine());
        System.out.println("Choroba");
        String diseaseStringVal = scanner.nextLine();
        Disease disease = Disease.valueOf(diseaseStringVal);

        Patient patient = new Patient();
        patient.setName(name);
        patient.setSurname(surname);
        patient.setHowAngry(howAngry);
        patient.setDisease(disease);
        System.out.println("Zarejestrowano pacjenta");
        return patient;
    }
}

