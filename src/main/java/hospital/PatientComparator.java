package hospital;

import java.util.Comparator;

public class PatientComparator implements Comparator<Patient> {

    private static final String KOWALSKI = "Kowalski";

    @Override
    public int compare(Patient o1, Patient o2) {
        boolean isFirstKowalski = o1.getSurname().equals(KOWALSKI);
        boolean isSecondKowalski = o2.getSurname().equals(KOWALSKI);
        if (!isFirstKowalski && isSecondKowalski) {
            return 1;
        } else if (isFirstKowalski && !isSecondKowalski) {
            return -1;
        }
        return 0;
    }
}
