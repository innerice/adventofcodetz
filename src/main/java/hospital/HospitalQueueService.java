package hospital;

import java.util.PriorityQueue;

public class HospitalQueueService {
    private PriorityQueue<Patient> hospitalQueue = new PriorityQueue<>();

    public void addPatient(Patient patient) {
    hospitalQueue.add(patient);
    }
    public Patient nextPatient(){
        return hospitalQueue.peek();
    }

    public Patient handlePatient(){
        return hospitalQueue.poll();
    }
    public int queueSize(){
        return hospitalQueue.size();
    }

}
